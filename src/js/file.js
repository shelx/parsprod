require('./bootstrap');
import Vue from 'vue/dist/vue.esm.js';
let userId = '3';
let host = "http://192.168.0.207:8080/api/users/" + userId;
const configuratorGame = {
    el: '#game',
    data() {
        return {
            userSelected: '',
            ui: {
                state: 'orc'
            },
            users: [],
            message: '',
            status: '',
            char: {},
            shop: [],
            firstStart: true
        }
    },
    created: function () {
        axios.get(host + '/users')
            .then(us => {
                this.users = us.data;
                console.log('z', us.data);

                axios.get(host)
                    .then(r => {
                        console.log('z', r.data);
                        this.char = r.data.char;
                        this.shop = r.data.shop;
                    });
            });

            setTimeout(() => {
                this.firstStart = false;
            }, 4000);
    },
    mounted: function () {
        console.log('monted');
    },

    watch: {
        
    },

    methods: {
        changeState: function(state) {
            if (state) {
                this.ui.state = state;
                return;
            }
            if (this.ui.state == 'orc') {
                this.ui.state = 'prop';
            } else {
                this.ui.state = 'orc';
            }
        },

        buy: function(id) {
            axios.post(host + '/buy', {item_id: id})
            .then(r => {
                console.log('z', r.data);
                this.char = r.data.char;
                this.shop = r.data.shop;
                this.ui.state = 'orc'
            });
        },

        battle: function() {
            if (!this.userSelected) {
                alert('Choose your opponent!');
                return;
            }

            axios.post(host + '/battle', {user_id: this.userSelected})
            .then(r => {
                console.log('z', r.data);
                this.char = r.data.char;
                this.shop = r.data.shop;
                this.message = r.data.message;
                this.stauts = r.data.status;
                this.ui.state = 'battle-message';
            });
        },

        del: function(item) {
            axios.delete(host + '/items/'+item)
            .then(r => {
                this.char = r.data.char;
                this.shop = r.data.shop;
                this.ui.state = 'inventory';
            });
        }

    }
};


$(document).ready(() => {
    window.vueConfig = new Vue(configuratorGame);
});

